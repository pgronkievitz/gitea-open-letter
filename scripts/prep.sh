#!/bin/bash
# prep.sh: Helper script to convert README.md in to zola index page content
# Copyright © 2022 Aravinth Manivannan <realaravinth@batsense.net>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

readonly file="content/letter.md"

set -euo pipefail

init() {
	rm -rf content || true

	mkdir content
	touch $file
	echo "+++" > $file
	echo 'title = "Gitea Open Letter"' >> $file
	echo "+++" >> $file
	echo "" >> $file
	cat README.md | tail -n +5 >> $file
}

prep_status() {
	mkdir content/updates

	index="content/updates/_index.md"
	touch $index
	echo "+++" > $index
	echo 'render = true' >> $index
	echo 'title = "Updates"' >> $index
	echo 'sort_by = "date"' >> $index
	echo 'template = "updates/index.html"' >> $index
	echo 'page_template = "updates/post.html"' >> $index
	echo 'generate_feed = true' >> $index
	echo "+++" >> $index
	echo "" >> $index

	python ./scripts/gen.py
}

init
prep_status
