This page is updated daily to reflect the latest evolution of the situation. It is updated by people who signed the Gitea Open Letter and reflect their point of view. Although the best efforts are made to present facts in a neutral way, people who did not sign the Gitea Open Letter would likely describe the events differently.

---

### Current status

* Figure out an [action plan](https://demo.hedgedoc.org/tJyU8W5ARk2xf2Rjb6DJqw#) for the Gitea Community to regain control of the project.

### October 30th

Ambassador(s) are engaged in discussions with the shareholders of the Gitea Ltd company and the members of the Gitea Community who signed the Gitea Open Letter. In the afternoon, a [second blog post is published](https://blog.gitea.io/2022/10/a-message-from-lunny-on-gitea-ltd.-and-the-gitea-project/) and signed by Lunny and techknowlogick.

The shareholders of Gitea Ltd do not agree to the demands of the Gitea Open Letter.

### October 29th

A second blog post is being prepared from the same authors as the original Gitea post.

### October 28th

The announcement of *Gitea Ltd* raised concerns from members of the Gitea community, who [published the Gitea Open Letter](https://gitea-open-letter.coding.social/). It was [advertised](https://codeberg.org/SocialCoding/gitea-open-letter/issues/19) on the fediverse, HN etc. 

### October 25th

A [blog post was written on the Gitea blog](https://blog.gitea.io/2022/10/open-source-sustainment-and-the-future-of-gitea/), informing about the creation of a company named *Gitea Ltd*.

---

All the discussions and documents archived: if you have any question feel free to ask [in the chatroom](https://matrix.to/#/#general-forgefed:matrix.batsense.net) or read the raw material (it is a lot but it is available publicly).
